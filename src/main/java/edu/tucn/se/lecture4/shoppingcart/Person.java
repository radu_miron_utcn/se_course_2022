package edu.tucn.se.lecture4.shoppingcart;

/**
 * @author Radu Miron
 * @version 1
 */
public class Person extends Customer {
    private String firstName;
    private String lastName;

    public Person(String address, String phone, String firstName, String lastName) {
        super(address, phone);
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
