package edu.tucn.se.lecture4.shoppingcart;

/**
 * @author Radu Miron
 * @version 1
 */
public class Company extends Customer {
    private String name;
    private String regNum;

    public Company(String address, String phone, String name, String regNum) {
        super(address, phone);
        this.name = name;
        this.regNum = regNum;
    }
}
