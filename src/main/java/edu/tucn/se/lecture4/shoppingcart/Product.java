package edu.tucn.se.lecture4.shoppingcart;

/**
 * @author Radu Miron
 * @version 1
 */
public class Product {
    private String name;
    private String price;

    public Product(String name, String price) {
        this.name = name;
        this.price = price;
    }
}
