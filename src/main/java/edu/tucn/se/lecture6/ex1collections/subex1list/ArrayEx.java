package edu.tucn.se.lecture6.ex1collections.subex1list;

import java.util.Arrays;

/**
 * @author Radu Miron
 * @version 1
 */
public class ArrayEx {
    public static void main(String[] args) {
        int[] array = new int[5];
        for (int i = 0; i < 5; i++) {
            array[i] = i;
        }

        //somewhere in the code
        array[5] = 22;

        Arrays.stream(array)
                .forEach(el -> System.out.println(el));
    }
}
