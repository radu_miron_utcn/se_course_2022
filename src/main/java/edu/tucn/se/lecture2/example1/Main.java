package edu.tucn.se.lecture2.example1;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        int a = 5;
        int b = 5;
        int c = 7;
        boolean test = true;
        System.out.println(a == b);

        String name = "John";

        System.out.println("\u263a \u263b \u263c \u263d");

        System.out.println(a++);
        System.out.println(++a);


        System.out.println("(a == b) " + (a == b));
        System.out.println("(a == c) " + (a == c));
        System.out.println("(a == b) && (a == c) " + ((a == b) && (a == c)));
        System.out.println("(a == b) || (a == c) " + ((a == b) || (a == c)));

        System.out.println("test " + a + "abc");
        System.out.println("test " + a + b);
        System.out.println("test " + (a + b));

        int d = (a == b) ? c : 0;
        int d1 = (a == 88) ? c : 0;

        System.out.println(d);
        System.out.println(d1);

        int[] v1 = {1, 2, 3};
//        v1[3] = 12;

        int v2[] = new int[10];
        v2[0] = 22;
        v2[1] = 12;

        for (int i = 0; i < v2.length; i++) {
            System.out.println(i + ": " + v2[i]);
        }

        if (b > 4) {
            System.out.println("n>4");
        }

        System.out.println("sasfs");
        c = b + a;

    }
}
