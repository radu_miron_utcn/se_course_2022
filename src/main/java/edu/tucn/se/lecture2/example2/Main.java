package edu.tucn.se.lecture2.example2;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Car car1 = new Car("red", 5, "Dacia Logan");
        Car car2 = new Car("blue", 5, "Renault Symbol");
        Car car3 = new Car("pink", 6, "Renault Megane");

        car1.move();
        car2.move();

        car1.crash(car2);
        car2.crash(car3);

//        Car.NUMBER_OF_WHEELS = 6;

        car1.setColor("green");


        System.out.println("car1 is " + car1.getColor());
    }
}
