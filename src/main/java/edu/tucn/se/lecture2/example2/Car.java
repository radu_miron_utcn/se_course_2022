package edu.tucn.se.lecture2.example2;

/**
 * @author Radu Miron
 * @version 1
 */
public class Car {
    public static final int NUMBER_OF_WHEELS = 4;

    String color;
    private int numberOfGears;
    private String model;

    public Car(String color, String model) {
        this.color = color;
        this.model = model;
    }

    public Car(String color, int numberOfGears, String model) {
        this.color = color;
        this.numberOfGears = numberOfGears;
        this.model = model;
    }

    static void printNumberOfWheels() {
        System.out.println("All cars have " + Car.NUMBER_OF_WHEELS + " wheels!");
    }

    public void move() {
        System.out.println("The " + this.color + " car moves");
    }

    public void move(int distance) {
        System.out.println("The " + this.color + " car moves " + distance + " m");
    }

    public void crash(Car another) {
        System.out.println("The " + this.color + " car crashes into " + another.color + " car");
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return this.color;
    }
}
