package edu.tucn.se.lecture3.ex1;

/**
 * @author Radu Miron
 * @version 1
 */
class Person { // superclass
    private String name;

    Person(String name) { // (2)
        System.out.println("Constructor of Person.");
        this.name = name;
    }

    public void printName() {
        System.out.println(this.name);
    }
}

class Student extends Person { // subclass
    Student(String name) {
        super(name); // (3)
        System.out.println("Constructor of Student.");
    }

    @Override
    public void printName() {
        System.out.println("This student's name is: ");
        super.printName();
    }
}

class Teacher extends Person { // subclass
    private String university;

    Teacher(String name, String university) {
        super(name); // (3)
        System.out.println("Constructor of Student.");
    }

    public void printUniversity(){
        System.out.println("This teacher's university is" + this);
    }
}

public class Main {
    public static void main(String[] args) {
        Student s1 = new Student("John"); // (1)
//        Student s2 = new Student("Maria"); // (1)
//
//        s1.printName();
//        s2.printName();
//
//        Teacher t1 = new Teacher("Jim", "TUCN");
//        t1.printName();
//        t1.printUniversity();

        Person p1 = s1;
        p1.printName();

        Person p2 = new Teacher("Nick", "");
//        p2.printUniversity();

        Teacher t1 = (Teacher) p1;
        t1.printUniversity();

    }
}
