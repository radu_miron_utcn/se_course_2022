package edu.tucn.se.lecture3.ex2;

import java.util.Scanner;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("Choose your car: 1. Type1, 2. Type2");

        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();

        Car car;

        switch (choice) {
            case 1:
                car = new Type1("Type1");
                break;

            case 2:
                car = new Type2("Type2");
                break;
            default:
                throw new IllegalArgumentException("Bad choice!");
        }

        car.start();
        car.moveForward();
        car.stop();

        System.out.println("Game over!");
    }
}
