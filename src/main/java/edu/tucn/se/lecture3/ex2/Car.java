package edu.tucn.se.lecture3.ex2;

/**
 * @author Radu Miron
 * @version 1
 */
public class Car {
    private String name;

    public Car(String name) {
        this.name = name;
    }

    public void start(){
        System.out.println(this.name + " starts");
    }

    public void moveForward(){
        System.out.print(this.name + " moves");
    }

    public void stop(){
        System.out.println(this.name + " stops");
    }
}
