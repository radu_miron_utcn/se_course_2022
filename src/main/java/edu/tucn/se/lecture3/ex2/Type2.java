package edu.tucn.se.lecture3.ex2;

/**
 * @author Radu Miron
 * @version 1
 */
public class Type2 extends Car {

    public Type2(String name) {
        super(name);
    }

    @Override
    public void moveForward() {
        super.moveForward();
        System.out.println(" faster");
    }
}
