package edu.tucn.se.lecture5.ex2interface;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Interf1 i1 = new Class1();
        Interf2 i2 = new Class1();

        i1.met1();
        i2.met2();
        i2.met3();

        Class1 class1 = new Class1();
        class1.met1();
        class1.met2();
        class1.met3();

        Class1 class2 = (Class1) i2;
        class2.met1();
        class2.met2();
        class2.met3();
    }
}
