package edu.tucn.se.lecture5.ex2interface;

/**
 * @author Radu Miron
 * @version 1
 */
public interface Interf1 {
    int a = 4;
    void met1();
}
