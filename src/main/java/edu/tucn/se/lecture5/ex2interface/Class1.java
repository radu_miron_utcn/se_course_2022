package edu.tucn.se.lecture5.ex2interface;

/**
 * @author Radu Miron
 * @version 1
 */
public class Class1 implements Interf1, Interf2 {
    @Override
    public void met1() {
        System.out.println("met1");
    }

    @Override
    public void met2() {
        System.out.println("met2");

    }

    @Override
    public void met3() {
        System.out.println("met3");
    }
}
