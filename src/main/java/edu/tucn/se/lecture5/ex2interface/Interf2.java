package edu.tucn.se.lecture5.ex2interface;

/**
 * @author Radu Miron
 * @version 1
 */
public interface Interf2 {
    void met2();
    void met3();
}
