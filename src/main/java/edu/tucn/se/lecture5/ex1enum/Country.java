package edu.tucn.se.lecture5.ex1enum;

/**
 * @author Radu Miron
 * @version 1
 */
public enum Country {
    ROMANIA,
    FRANCE,
    UKRAINE
}
