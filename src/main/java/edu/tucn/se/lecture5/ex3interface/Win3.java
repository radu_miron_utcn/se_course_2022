package edu.tucn.se.lecture5.ex3interface;

import javax.swing.*;
import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class Win3 extends JFrame {
    Win3(int a, int b) {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setBounds(a, b, 400, 100);
//        this.setLayout(null);
        JButton b1 = new JButton("Click to close the window");

        b1.addActionListener(x -> {
            System.out.println(x.getActionCommand());
            int x1 = new Random().nextInt(1800);
            int y1 = new Random().nextInt(800);
            new Win3(x1, y1);
        });

        this.add(b1);
        this.setVisible(true);
    }
}
