package edu.tucn.se.lecture5.ex3interface;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class Win1 extends JFrame {
    Win1(int x, int y) {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setBounds(x, y, 400, 100);
//        this.setLayout(null);
        JButton b1 = new JButton("Click to close the window");
        ButtonHandler buttonHandler = new ButtonHandler();
        b1.addActionListener(buttonHandler);
        this.add(b1);
        this.setVisible(true);
    }


    class ButtonHandler implements ActionListener {
        public void actionPerformed(ActionEvent actionEvent) {
            int x = new Random().nextInt(1800);
            int y = new Random().nextInt(800);
            new Win1(x, y);
        }
    }
}
