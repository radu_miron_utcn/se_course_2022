package edu.tucn.se.lecture5.ex3interface;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class Win2 extends JFrame {
    Win2(int x, int y) {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setBounds(x, y, 400, 100);
//        this.setLayout(null);
        JButton b1 = new JButton("Click to close the window");

        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int x = new Random().nextInt(1800);
                int y = new Random().nextInt(800);
                new Win2(x, y);
            }
        });

        this.add(b1);
        this.setVisible(true);
    }
}
