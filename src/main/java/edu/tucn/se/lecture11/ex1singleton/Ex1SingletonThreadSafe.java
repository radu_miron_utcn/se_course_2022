package edu.tucn.se.lecture11.ex1singleton;

import java.util.List;

/**
 * @author Radu Miron
 */
public class Ex1SingletonThreadSafe {
    public static void main(String[] args) {
        FileWriterThreadSafe fileWriter1 = FileWriterThreadSafe.getInstance();
        fileWriter1.setFileName("test1");

        System.out.println("After the first call of getInstance()");
        System.out.println(fileWriter1.getFileName());

        FileWriterThreadSafe fileWriter2 = FileWriterThreadSafe.getInstance();
        fileWriter2.setFileName("test2");

        System.out.println("After the second call of getInstance()");
        System.out.println("fileWriter1.fileName: " + fileWriter1.getFileName());
        System.out.println("fileWriter2.fileName: " + fileWriter2.getFileName());

        System.out.println("fileWriter1 == fileWriter2? " + (fileWriter1 == fileWriter2));
    }
}

class FileWriterThreadSafe {
    private static volatile FileWriterThreadSafe instance;
    private String fileName;

    private FileWriterThreadSafe() {
    }

    public static synchronized FileWriterThreadSafe getInstance() {
        if (instance == null) {
            instance = new FileWriterThreadSafe();
        }

        return instance;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void writeLines(List<String> lines) {
        //todo: implement
    }
}