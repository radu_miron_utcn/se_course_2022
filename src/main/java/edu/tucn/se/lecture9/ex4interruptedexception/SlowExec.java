package edu.tucn.se.lecture9.ex4interruptedexception;

/**
 * @author Radu Miron
 * @version 1
 */
public class SlowExec extends Thread {
    @Override
    public void run() {
        this.setName("SlowExec");

        System.out.println(Thread.currentThread().getName() +
                ": start the execution");

        try {
            Thread.sleep(24 * 60 * 60 * 1000);
        } catch (InterruptedException e) {
            System.err.println(this.getName() + ": I've been interrupted");
        }

        System.out.println(Thread.currentThread().getName() +
                ": ended the execution");
    }
}
