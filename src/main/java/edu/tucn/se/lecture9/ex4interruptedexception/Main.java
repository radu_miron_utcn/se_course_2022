package edu.tucn.se.lecture9.ex4interruptedexception;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) throws InterruptedException {
        SlowExec slowExec = new SlowExec();
        slowExec.start();

        System.out.println("Wait 5 sec then interrupt slowExec thread");
        Thread.sleep(5000);

        slowExec.interrupt();
    }
}
