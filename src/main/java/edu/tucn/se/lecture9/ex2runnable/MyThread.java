package edu.tucn.se.lecture9.ex2runnable;

/**
 * @author Radu Miron
 * @version 1
 */
public class MyThread implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 20; i++) {
            System.out.println(Thread.currentThread().getName() +" message " + i);

            try {
                Thread.sleep(20);
            } catch (InterruptedException ignore) {
            }
        }
    }

    public void start(){
        new Thread(this).start();
    }
}
