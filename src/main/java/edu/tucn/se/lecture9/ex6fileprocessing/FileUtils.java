package edu.tucn.se.lecture9.ex6fileprocessing;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class FileUtils {

    private FileUtils() {
    }

    public static void generateFile(String filePath) {
        try (FileWriter fileWriter = new FileWriter(filePath)) {
            for (int i = 0; i < 40000000; i++) {
                fileWriter.write("" + new Random().nextInt(Integer.MAX_VALUE));
            }

            fileWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Map<String, Integer> processFile(String path) {
        Map<String, Integer> results = new HashMap<>();
        try {
            Files.lines(Paths.get(path))
                    .forEach(l -> {
                        for (int i = 0; i < l.length(); i++) {
                            String key = l.charAt(i) + "";
                            Integer value = results.get(key);

                            if (value == null) {
                                value = 1;
                            } else {
                                value++;
                            }

                            results.put(key, value);
                        }
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }

        return results;
    }

//    public static void main(String[] args) {
//        String filePath = "testfiles/numbers100.txt";
//        FileUtils.generateFile(filePath);
//        long t1 = System.currentTimeMillis();
//        Map<String, Integer> results = FileUtils.processFile(filePath);
//        long t2 = System.currentTimeMillis();
//
//        System.out.println((t2-t1)/1000);
//    }
}
