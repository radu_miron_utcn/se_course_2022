package edu.tucn.se.lecture9.ex6fileprocessing;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

/**
 * @author Radu Miron
 * @version 1
 */
public class Win extends JFrame {
    Win() {
        setBounds(100, 100, 550, 550);
        setLayout(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JLabel fileNamelabel = new JLabel("File name");
        fileNamelabel.setBounds(20, 20, 100, 20);

        JTextField fileNameInput = new JTextField();
        fileNameInput.setBounds(140, 20, 200, 20);

        JButton fileProcButton = new JButton("Process");
        fileProcButton.setBounds(360, 20, 170, 20);

        JTextArea results = new JTextArea();
        results.setBounds(20, 60, 400, 400);

        fileProcButton.addActionListener(new HandleProcButton(fileNameInput, results));

        add(fileNamelabel);
        add(fileNameInput);
        add(fileProcButton);
        add(results);

        setVisible(true);
    }

    class HandleProcButton implements ActionListener {
        private JTextField fileNameInput;
        private JTextArea resultsTextArea;

        public HandleProcButton(JTextField fileNameInput, JTextArea resultsTextArea) {
            this.fileNameInput = fileNameInput;
            this.resultsTextArea = resultsTextArea;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            String filePath = fileNameInput.getText();
            System.out.println(Thread.currentThread().getName());
            new Thread(() -> {
                Map<String, Integer> results = FileUtils.processFile(filePath);
                results.forEach((k, v) -> {
                    resultsTextArea.append(k + " -> " + v + "\n");
                });
            }).start();

            //or
//
//            for (Map.Entry<String, Integer> kvPair : results.entrySet()) {
//                resultsTextArea.append(kvPair.getKey() + " -> " + kvPair.getValue() + "\n");
//            }

        }
    }
}
