package edu.tucn.se.lecture9.ex5singleton;

/**
 * @author Radu Miron
 * @version 1
 */
public class Singleton {
    private static volatile Singleton instance;
    int a, b;
    String s;

    private Singleton() {
    }

    public static synchronized Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }

        return instance;
    }
}
