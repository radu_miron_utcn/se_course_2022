package edu.tucn.se.lecture9.ex3lambda;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        new Thread(() -> run()).start();
        new Thread(() -> run()).start();
        run();
    }

    public static void run() {
        for (int i = 0; i < 20; i++) {
            System.out.println(Thread.currentThread().getName() +" message " + i);

            try {
                Thread.sleep(20);
            } catch (InterruptedException ignore) {
            }
        }
    }
}
