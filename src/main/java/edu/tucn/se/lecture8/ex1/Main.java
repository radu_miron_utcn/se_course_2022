package edu.tucn.se.lecture8.ex1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    private static final String CLICK_HERE = "Click here";
    private static final String CLICK_HERE_2 = "Click here 2";

    public static void main(String[] args) {
        JFrame frame = new JFrame("Hello Swing!");
        frame.setLayout(new GridLayout(1, 3));
        frame.setBounds(100, 100, 400, 100);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        JButton button1 = new JButton();
        button1.setText(CLICK_HERE);
        button1.addActionListener(new ButtonClickHandler(null));

        JButton button2 = new JButton();
        button2.setText(CLICK_HERE_2);

        JTextField myTextField = new JTextField();

        button2.addActionListener(new ButtonClickHandler(myTextField));

        frame.add(button1);
        frame.add(button2);
        frame.add(myTextField);
        frame.setVisible(true);

    }

    static class ButtonClickHandler implements ActionListener {
        private JTextField tf;

        ButtonClickHandler(JTextField tf) {
            this.tf = tf;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            int num = new Random().nextInt(Integer.MAX_VALUE);

            if (actionEvent.getActionCommand().equals(CLICK_HERE_2)) {
                tf.setText("" + num);
            } else if (actionEvent.getActionCommand().equals(CLICK_HERE)) {
                System.out.println(num);
            } else { // this should never happen
                throw new IllegalArgumentException("Unknown click source");
            }
        }
    }
}
