package edu.tucn.se.lecture12.ex1;

import org.apache.commons.io.FileUtils;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @author Radu Miron
 * @version 1
 */
public class RefresherThread extends Thread {
    private JTextField fileNameInput;
    private JTextArea contentContainer;

    public RefresherThread(JTextField fileNameInput, JTextArea contentContainer) {
        this.fileNameInput = fileNameInput;
        this.contentContainer = contentContainer;
    }

    @Override
    public void run() {
        while (true) {
            try {
                String fileContent = FileUtils.readFileToString(new File(fileNameInput.getText()), StandardCharsets.UTF_8);
                contentContainer.setText(fileContent);
                Thread.sleep(10000);
            } catch (IOException | InterruptedException e) {
                contentContainer.setText("An error has occurred while reading the file");
            }
        }
    }
}
