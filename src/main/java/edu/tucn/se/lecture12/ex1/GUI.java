package edu.tucn.se.lecture12.ex1;

import javax.swing.*;

/**
 * @author Radu Miron
 * @version 1
 */
public class GUI extends JFrame {
    private JTextField fileNameInput;
    private JTextArea fileContentContainer;
    private JButton startButton;

    public GUI() {
        this.setLayout(null);
        this.setSize(240, 440);
        this.fileNameInput = new JTextField("file name");
        this.fileNameInput.setBounds(20, 20, 200, 20);

        this.fileContentContainer = new JTextArea("file content");
        this.fileContentContainer.setBounds(20, 60, 200, 300);

        this.startButton = new JButton("Start");
        this.startButton.setBounds(20, 380, 200, 20);
        this.startButton.addActionListener(e ->
                new RefresherThread(getFileNameInput(), getFileContentContainer()).start());

        this.add(fileNameInput);
        this.add(fileContentContainer);
        this.add(startButton);

        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        this.setVisible(true);
    }

    public JTextField getFileNameInput() {
        return fileNameInput;
    }

    public JTextArea getFileContentContainer() {
        return fileContentContainer;
    }
}
