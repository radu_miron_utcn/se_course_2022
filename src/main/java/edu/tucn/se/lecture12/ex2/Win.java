package edu.tucn.se.lecture12.ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * @author Radu Miron
 * @version 1
 */
public class Win extends JFrame {
    private ArrayBlockingQueue taskQueue;
    private JTextArea results;

    Win(ArrayBlockingQueue taskQueue) {
        this.taskQueue = taskQueue;

        setBounds(100, 100, 550, 550);
        setLayout(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JLabel fileNamelabel = new JLabel("File name");
        fileNamelabel.setBounds(20, 20, 100, 20);

        JTextField fileNameInput = new JTextField();
        fileNameInput.setBounds(140, 20, 200, 20);

        JButton fileProcButton = new JButton("Process");
        fileProcButton.setBounds(360, 20, 170, 20);

        results = new JTextArea();
        results.setBounds(20, 60, 400, 400);

        fileProcButton.addActionListener(new HandleProcButton(fileNameInput, results));

        add(fileNamelabel);
        add(fileNameInput);
        add(fileProcButton);
        add(results);

        setVisible(true);
    }

    public JTextArea getResults() {
        return results;
    }

    class HandleProcButton implements ActionListener {
        private JTextField fileNameInput;
        private JTextArea resultsTextArea;

        public HandleProcButton(JTextField fileNameInput, JTextArea resultsTextArea) {
            this.fileNameInput = fileNameInput;
            this.resultsTextArea = resultsTextArea;
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            String filePath = fileNameInput.getText();
            System.out.println(Thread.currentThread().getName());

            try {
                taskQueue.put(filePath);
            } catch (InterruptedException ignored) {
            }

            //or
//
//            for (Map.Entry<String, Integer> kvPair : results.entrySet()) {
//                resultsTextArea.append(kvPair.getKey() + " -> " + kvPair.getValue() + "\n");
//            }

        }
    }


}
