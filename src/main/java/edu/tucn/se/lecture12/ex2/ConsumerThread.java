package edu.tucn.se.lecture12.ex2;

import javax.swing.*;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * @author Radu Miron
 * @version 1
 */
public class ConsumerThread extends Thread {
    private ArrayBlockingQueue<String> taskQueue;
    private JTextArea resultsTextArea;

    public ConsumerThread(JTextArea resultsTextArea, ArrayBlockingQueue<String> taskQueue) {
        this.taskQueue = taskQueue;
        this.resultsTextArea = resultsTextArea;
    }

    @Override
    public void run() {
        while (true){
            String filePath = null;

            try {
                filePath = taskQueue.take();
            } catch (InterruptedException e) {
            }

            System.out.println(Thread.currentThread().getName());

            Map<String, Integer> results = FileUtils.processFile(filePath);
                results.forEach((k, v) -> {
                    resultsTextArea.append(k + " -> " + v + "\n");
                });
        }
    }
}
